/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AndroidAction.Controller;

import AndroidAction.Data.ActionIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author thomsonverghese
 */
public class ViewByActionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BroadcastActionDescServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BroadcastActionDescServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = null;
        String webINFDirectoryPath = request.getServletContext().getRealPath("/WEB-INF/");
        if (webINFDirectoryPath != null) {
            Logger.getLogger(ViewByActionController.class.getName()).log(Level.SEVERE, "directory created " + webINFDirectoryPath);
            getServletContext().log("directory created " + webINFDirectoryPath);
        } else {
            Logger.getLogger(ViewByActionController.class.getName()).log(Level.SEVERE, "directory not created: "+webINFDirectoryPath);
        }
        HashMap<String, String> actionDescriptionMap = null;
        String action = request.getParameter("action");
        if (action == null) {
            url = "/view_by_action.jsp";
            actionDescriptionMap = ActionIO.getDescriptionMap(this.getServletContext());
            request.setAttribute("actions", actionDescriptionMap);
        } else if (action.equals("Submit")) {

            String actionSelected = request.getParameter("AndroidAction");
            actionDescriptionMap = ActionIO.getDescriptionMap(this.getServletContext());
            request.setAttribute("actions", actionDescriptionMap);

            request.setAttribute("actionDescription", actionDescriptionMap.get(actionSelected));
            request.setAttribute("selectedAction", actionSelected);
            request.setAttribute("apiListString", ActionIO.getAPIListStringForAction(this.getServletContext(), actionSelected));
            url = "/view_by_action.jsp";

        }

        // forward request and response objects
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
