/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AndroidAction.Controller;

import AndroidAction.Business.Action;
import AndroidAction.Data.ActionIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author thomsonverghese
 */
public class ViewByAPIController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewByAPIServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewByAPIServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       String url = "/index.html";
        HashMap<String, String> actionsMap = null;
        String action = request.getParameter("action");
        if (action == null) {
            url = "/view_by_api.jsp";
        }
        else if (action.equals("SubmitAPI")) {
            
            //get the selected API level and pass it to teh jsp page
             String selectedAPI = request.getParameter("API");
            request.setAttribute("selectedAPI", Integer.parseInt(selectedAPI));
            
            ArrayList<Action> actionList = ActionIO.getActionList(this.getServletContext(), Integer.parseInt(selectedAPI));

            request.setAttribute("actionList", actionList);
            url = "/view_by_api.jsp";

        }  else if (action.equals("SubmitAction")) {
            //need to get the selected API
            String selectedAPI = request.getParameter("selectedAPI");
            request.setAttribute("selectedAPI", Integer.parseInt(selectedAPI));
            
            ArrayList<Action> actionList = ActionIO.getActionList(this.getServletContext(), Integer.parseInt(selectedAPI));

            String selectedAction = request.getParameter("androidAPIAction");
            
            String actionDescription = ActionIO.getDescriptionMap(this.getServletContext()).get(selectedAction);
           
            request.setAttribute("actionList", actionList);  
            request.setAttribute("selectedAction", selectedAction);
            request.setAttribute("actionDescription", actionDescription);
            url = "/view_by_api.jsp";
        }
        

        // forward request and response objects
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
