/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AndroidAction.Data;

import AndroidAction.Business.Action;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;


/**
 *
 * @author thomsonverghese
 */
public class ActionIO implements Serializable {

    public ActionIO() {

    }

    public static ArrayList<Integer> getAPILevels() {
        ArrayList<Integer> apiList = new ArrayList<>();
        apiList.add(2);
        apiList.add(3);
        apiList.add(4);
        apiList.add(5);
        apiList.add(6);
        apiList.add(7);
        apiList.add(8);
        apiList.add(9);
        apiList.add(10);
        apiList.add(11);
        apiList.add(12);
        apiList.add(13);
        apiList.add(14);
        apiList.add(15);
        apiList.add(16);
        apiList.add(17);
        apiList.add(18);
        apiList.add(19);
        apiList.add(20);
        apiList.add(21);
        return apiList;
    }
    public static ArrayList<Integer> getAPILevels(ServletContext sc) {

        ArrayList<Integer> apiList = new ArrayList<>();
        Set<String> pathList = sc.getResourcePaths("/WEB-INF/");
        for (String path : pathList) {
            String fileName = null;
            if (path.indexOf(".") > 0) {
                fileName = path.substring(path.lastIndexOf('/') + 1);
              fileName =  fileName.substring(0, fileName.indexOf('.'));
                if (fileName.contains("broadcast_actions")) {
                    fileName = fileName.replaceAll("[a-zA-Z_]", "");
                    apiList.add(Integer.parseInt(fileName));
                }
            }

        }

//        apiList.add(2);
//        apiList.add(3);
//        apiList.add(4);
//        apiList.add(5);
//        apiList.add(6);
//        apiList.add(7);
//        apiList.add(8);
//        apiList.add(9);
//        apiList.add(10);
//        apiList.add(11);
//        apiList.add(12);
//        apiList.add(13);
//        apiList.add(14);
//        apiList.add(15);
//        apiList.add(16);
//        apiList.add(17);
//        apiList.add(18);
//        apiList.add(19);
//        apiList.add(20);
//        apiList.add(21);
        Collections.sort(apiList, new Comparator<Integer>() {
        @Override
        public int compare(Integer i1, Integer i2) {
            return i1.compareTo(i2);
        }
    });
        return apiList;
    }

    public static String getAPIFile(int apiLevel) {
        return ("broadcast_actions" + Integer.toString(apiLevel) + ".txt");
    }

    public static String getDescriptionFile() {
//                String path = sc.getRealPath("/WEB-INF/products.txt");
        return ("broadcast_description" + ".txt");
    }

    public static HashMap<String, String> getDescriptionMap(ServletContext sc) throws IOException {
        try {
            HashMap<String, String> descriptionMap = null;
//            File descriptionFile = new File(serverContextFilePath + "/" + getDescriptionFile());
            
            InputStream iStream = sc.getResourceAsStream("/WEB-INF/"+getDescriptionFile());
            BufferedReader descriptionFileIn = null;

            descriptionFileIn = new BufferedReader(
                    new InputStreamReader(iStream));

            String descriptionLine = null;

            descriptionLine = descriptionFileIn.readLine();
            while (descriptionLine != null) {
                if (descriptionMap == null) {
                    descriptionMap = new HashMap<>();
                }
                StringTokenizer t = new StringTokenizer(descriptionLine, ";");
                String actionName = t.nextToken();
                String actionDesc = t.nextToken();
                descriptionMap.put(actionName, actionDesc);

                descriptionLine = descriptionFileIn.readLine();

            }
            descriptionFileIn.close();
            return descriptionMap;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ActionIO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public static ArrayList<Action> getActionList(ServletContext sc, int apiLevel) {
        ArrayList<Action> actionList = new ArrayList<>();
        HashMap<String, String> descHashMap = null;
        try {
            descHashMap = getDescriptionMap(sc);
        } catch (IOException ex) {
            Logger.getLogger(ActionIO.class.getName()).log(Level.SEVERE, null, ex);
        }

//        File actionsFile = new File(serverContextFilePath + "/" + getAPIFile(apiLevel));
        InputStream iStream = sc.getResourceAsStream("/WEB-INF/"+getAPIFile(apiLevel));

        try {
            BufferedReader actionFileIn;
            actionFileIn = new BufferedReader(
                    new InputStreamReader(iStream));

            String line = actionFileIn.readLine();
            while (line != null) {
                Action action = new Action();
                action.setName(line);

                //set description for the action
                action.setDescription(descHashMap.get(line));
                line = actionFileIn.readLine();
                actionList.add(action);
            }
            actionFileIn.close();

        } catch (IOException e) {
            System.err.println(e);
            return null;
        }
        return actionList;
    }

    public static String getAPIListStringForAction(ServletContext sc, String actionString) {
        ArrayList<Integer> apiList = getAPILevels();
        ArrayList<String> actionAPIList = new ArrayList<>();
        for (Integer apiLevel : apiList) {
            ArrayList<Action> actionList = getActionList(sc, apiLevel);
            for (Action action : actionList) {
                if (actionString.equals(action.getName())) {
                    actionAPIList.add(Integer.toString(apiLevel));
                }
            }

        }
        String returnString = null;
        int actionAPIListSize = actionAPIList.size();
        if (actionAPIListSize > 0) {
            returnString = "API "+ actionAPIList.get(0);
            for (int i = 1; i< actionAPIListSize; i++) {
                returnString = returnString + ", API " + actionAPIList.get(i);
            }
        } else {
            returnString = "Listed in none of the API's";
        }
        return returnString;
    }
}
