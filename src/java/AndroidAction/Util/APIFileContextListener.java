/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AndroidAction.Util;

import AndroidAction.Data.ActionIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author thomsonverghese
 */
public class APIFileContextListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
      ServletContext sc = sce.getServletContext();
        sc.setAttribute("apiList", ActionIO.getAPILevels(sc));
      
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
      
    }
    
}
