<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Welcome to Android Broadcast Actions Documentation</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <header>
            <h1>Welcome to Android Broadcast Actions Documentation</h1>
            <div class="top_right">
                <a href="index.html">Admin</a>
            </div>
        </header>
        
        <section>
            <nav id="navigation_bar">
                <ul>
                    <li>
                        <a href="BroadcastActionDescServlet" id="selected">View By Action</a><br>
                    </li>
                    <li>
                        <a href="ViewByAPIServlet">View By API</a><br>
                    </li>
                    <li>
                        <a href=" ">Difference</a><br>
                    </li>
                    <li>
                        <a href="">Intersection</a><br>
                    </li>
                </ul>
            </nav>
        </section>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
        <aside>
            <div style="float: left;">
            <form name="Submit" action="BroadcastActionDescServlet" method="post">  
                <select name="AndroidAction">
                <c:forEach var="item" items="${actions}">
                    <option value="${item.key}" ${item.key == selectedAction ? 'selected="selected"' : ''}>${item.key}</option>
                </c:forEach>
                </select> <br> <br>
                <input type="submit" value="Submit" name="action"  />
            </form>
            </div>
            <div style="float: right;">
            <p> <b> Description: </b></p>
            <p>${actionDescription}</p>
            <p> <b> API list: </b></p>
            <p>${apiListString}</p>
            </div>
        </aside>

        <footer>
            Copyright 2015
        </footer>

    </body>
</html>
