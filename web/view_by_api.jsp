<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Welcome to Android Broadcast Actions Documentation</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <header>
            <h1>Welcome to Android Broadcast Actions Documentation</h1>
            <div class="top_right">
                <a href="index.html">Admin</a>
            </div>
        </header>
        <section>
            <nav id="navigation_bar">
                <ul>
                    <li>
                        <a href="BroadcastActionDescServlet">View By Action</a><br>
                    </li>
                    <li>
                        <a href="ViewByAPIServlet" id="selected">View By API</a><br>
                    </li>
                    <li>
                        <a href=" ">Difference</a><br>
                    </li>
                    <li>
                        <a href="">Intersection</a><br>
                    </li>
                </ul>
            </nav>
        </section>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
        <aside>
            <form name="SubmitAPI" action="ViewByAPIServlet" method="post">
                <div style="float: left;">

                    <select name="API">
                <c:forEach var="item" items="${apiList}">
                    <option value="${item}" ${item == selectedAPI ? 'selected="selected"' : ''}>API ${item}</option>
                </c:forEach>
                    </select> <br> <br>
                    <input type="hidden" name="action" value="SubmitAPI">
                    <input type="submit" value="Submit" />

                </div>
            </form>
            <form>
                <div style="float: right;">

                  <select name="androidAPIAction">
                <c:forEach var="item" items="${actionList}">
                    <option value="${item.name}" ${item.name == selectedAction ? 'selected="selected"' : ''}> ${item.name}</option>
                </c:forEach>
                    </select> <br> <br>
<!--                    pass the selected API through-->
                   <input type="hidden" name="selectedAPI" value=${selectedAPI}>
                    <input type="hidden" name="action" value="SubmitAction">
                    <input type="submit" value="Submit"  />
                    <p> <b> Description: </b></p>
                    <p>${actionDescription}</p>
                </div>
            </form>
        </aside>

        <footer>
            Copyright 2015
        </footer>

    </body>
</html>
